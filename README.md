## Introduction

Web app to teach and visualize various topics in AI such as search algorithms, minimax, Q-learning, Baysean networks, and neural networks.

## Project Structure

### Frontend

Made with ReactJS

### Backend

Made with Express.

## Content

### Search algorithms

BFS, DFS, Djikstra's, and Astar, visualized in an editable maze.

### Minimax

Tic-tac-toe AI that uses minimax to find the optimal move and to demonstrate pruning.

### Q-learning

Pacman AI using approximate Q-learning to make decisions.

### Baysean networks

Graphs to visualize independence between nodes of a baysean network

### Neural networks

3 layer neural network with 100 nodes per layer using the GELU activation function to classify written numbers. Shows strength of connections between neurons changing as network is being trained.
